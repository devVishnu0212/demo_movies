import React from 'react';

const ShowTitleCard = ({key,title,image,releaseDate,overview}) => {
    return (
        <div className="show__card" key={key}>
            <img src={`${image}`} alt={title} />
            <div className="title__box">
                <h5>
                    {title}
                </h5>
                <p>
                    {releaseDate}
                </p>
            </div>

            <div className="overview">
                <h4 style={{fontWeight:"bold",padding:"10px 0"}}>
                   Overview 
                </h4>
                <p>
                    {overview}
                </p>
            </div>
        </div>
    );
}

 

export default ShowTitleCard;
import React,{useState,useEffect} from 'react';
import * as movies from '../api/movies.json'
import ShowTitleCard from './ShowTitleCard'
import Loader from './Loader'

import {
    useParams
  } from "react-router-dom";

const ShowDetails = () => {
    let { id } = useParams();
    const [state, setState] = useState([])
    const [load, setLoad] = useState(true)

    const fetchJson = async () => {
        let response = await movies.entries.sort(function(a, b) {
            return a.title.localeCompare(b.title);
        });
        setState(response)
        setLoad(false)
    }

    const filterJson = state.filter((item) => {
        return item.programType === `${id}` && item.releaseYear >= 2010
    })

    useEffect(() => {
        fetchJson()
    }, [])
   
    if(load){
        return (
            <Loader />
        );
    }
    return (  
        <div className="container_40 card__section">
            {
               filterJson.slice(0,21).map((data, i) => {
                    return (
                        <ShowTitleCard key={i} title={data.title} image={data.images.PosterArt.url} releaseDate={data.releaseYear} overview={data.description} />
                    );
                })
            }
        </div>
    );
}

export default ShowDetails;
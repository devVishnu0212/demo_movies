import React from 'react'
import Facebook from '../assets/social/facebook-white.svg'
import Instagram from '../assets/social/instagram-white.svg'
import Twitter from '../assets/social/twitter-white.svg'
import Appstore from '../assets/store/app-store.svg'
import Playstore from '../assets/store/play-store.svg'
import Windowstore from '../assets/store/windows-store.svg'

const Footer = () => {
    return( 
      <footer className='container_40'>
          <div className='footer_menu'> 
            <ul>
              <li>
                Home
              </li>
              <li>
                |
              </li>
              <li>
                About Us
              </li>
              <li>
                |
              </li>
              <li>
                Contact Us
              </li>
            </ul>
          </div>
          <div className="image"> 
            <div className="social">
              <img src={Facebook} alt="Facebook" />
              <img src={Instagram} alt="Instagram" />
              <img src={Twitter} alt="Twitter" />
            </div>
            <div className="store">
              <img src={Appstore} alt="Appstore" />
              <img src={Playstore} alt="Playstore" />
              <img src={Windowstore} alt="Windowstore" />
            </div>
          </div>
      </footer>
    );
}

export default Footer;
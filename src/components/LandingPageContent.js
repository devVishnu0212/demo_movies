import React from 'react'
import {
    Link
  } from "react-router-dom";

const PopularTitles = ({name, link}) => {
    return(
        <div className="card__titles">
            <h3>
                <Link to={`/${link}`}>
                  {name}
                </Link>
            </h3>
        </div>
    );
}

const LandingPageContent = () => {
    return(
        <div>
            <h2 style={{textAlign: "center",margin:"16px 0",}}>Popular Titles</h2>
            <div className="popular__titles">
                <PopularTitles name = "Popular Movies" link="movie" />
                <PopularTitles name = "Popular Tv Series" link="series" />
            </div>
        </div>
    );
}

export default LandingPageContent;
import React from 'react'
import {
    Link
  } from "react-router-dom";

const Header = () => {
    return(
        <nav>
            <div>
                <Link to='/' className="brand__name">
                    DemoLogo
                </Link>
            </div>
            <ul>
                <li>
                    <Link to='/login'>
                        Login
                    </Link>
                </li>
                <li>
                    <Link to='/signup'>
                        Sign Up
                    </Link>
                </li>
               
            </ul>
        </nav>
    );
}

export default Header;
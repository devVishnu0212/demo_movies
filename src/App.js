import React from 'react'
import LandingPageContent from './components/LandingPageContent'
import './App.css';

function App() {
  return (
    <div style={{minHeight:"80vh"}}>
      <LandingPageContent />
    </div>
  );
}

export default App;
